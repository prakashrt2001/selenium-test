*** Settings ***
Library           Collections
Library           RequestsLibrary
Library           OperatingSystem
Library           json

*** Test Cases ***
policyhealthcheck
    ${auth}=    Create List    healthcheck    zb!XztG34
    ${session}=    Create Session    policySession    https://172.23.0.4:6969    auth=${auth}
    ${header}=    Create Dictionary    Accept=application/json    Content-Type=application/json
    ${response}=    Get Request    policySession    /policy/api/v1/healthcheck    headers=${header}
    Log    ${response.text}
    Should Be Equal As Strings    ${response.status_code}    200


get_policies
    ${auth}=    Create List    healthcheck    zb!XztG34
    ${session}=    Create Session    policySession    https://172.23.0.4:6969    auth=${auth}
    ${header}=    Create Dictionary    Accept=application/json    Content-Type=application/json
    Comment    get the list of policies
    ${response}=    Get Request    policySession    /policy/api/v1/policytypes/onap.policies.controlloop.operational.common.Drools/versions/1.0.0/policies    headers=${header}
    Should Be Equal As Strings    ${response.status_code}    200
    Log    policies ${response.text}

