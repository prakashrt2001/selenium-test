*** Settings ***
Library           Selenium2Library
Library           RequestsLibrary
Library           OperatingSystem
Library           json

*** Variables ***
${Base_URL}       http://thetestingworldapi.com/
${REQBIN_URL}     https://reqbin.com/
${Content_URL}    https://cdn.contentful.com

*** Test Cases ***
maths
    ${result}=    add    5    10
    Log    ${result}
    Run Keyword If    ${result}==15    Pass Execution    Addition Passed

login_test
    [Documentation]    Invoke browser
    ...    launch web page
    ...    login
    Open Browser    https://www.tutorialspoint.com/    chrome
    Maximize Browser Window
    Comment    Sleep    1
    Element Should Be Visible    //span[text()="eBooks"]
    Click Element    //span[text()="eBooks"]
    Element Should Be Visible    //*[@id="search-strings"]
    Click Element    //*[@id="search-strings"]
    Input Text    //*[@id="search-strings"]    python
    Comment    Click Button    //*[@class="svg-inline--fa fa-search fa-w-16"]
    Press Key    //*[@id="search-strings"]    \\13

API_GET_test
    [Tags]    APIGET
    Create Session    Get_Student_Details    ${Base_URL}
    ${response}=    Get Request    Get_Student_Details    api/studentsDetails
    Log    ${response.status_code}
    Log    ${response.content}
    Comment    Create Student Record
    ${headers}=    Create Dictionary    Content-Type=application/json
    ${body}=    Create Dictionary    first_name=lakshmi    middle_name=Deepa    last_name=vandadi    date_of_birth=15/04/1983
    ${response}=    Post Request    Get_Student_Details    api/studentsDetails    data=${body}    headers=${headers}
    ${status_code}=    Convert To String    ${response.status_code}
    Should Be Equal    ${status_code}    201    Return codes are equal
    Log    ${response.status_code}
    ${response}=    Get Request    Get_Student_Details    api/studentsDetails/
    Log    ${response.status_code}
    Log    ${response.content}

API_GET_StudentID_Details_Test
    [Tags]    API
    Create Session    the_api_test    ${Base_URL}
    ${response}=    Get Request    the_api_test    api/studentsDetails/177946
    Log    ${response.status_code}
    Log    ${response.content}
    ${response_status_code_str}=    Convert To String    ${response.status_code}
    Should Be Equal    ${response_status_code_str}    200
    Comment    API_Delete_Student
    API_Put_Student

reqbin_test
    Comment    API test of reqbin web site
    ${session}=    Create Session    reqbin_session    ${REQBIN_URL}
    ${header}=    Create Dictionary    Content-Type=application/json
    ${body}=    Create Dictionary    Id=78912    Customer=Jason Sweet    Quantity=1    Price=18.00
    ${response}=    Post Request    reqbin_session    echo/post/json    ${header}    ${body}
    ${status_code}=    Convert To String    ${response.status_code}
    Should Be Equal As Strings    ${status_code}    200
    Log    ${response.text}
    Should Contain    ${response.text}    true
    Comment    POST Request using json input
    Comment    Read input from json file
    ${postjson}=    Get File    ${CURDIR}/APIInputData.json
    ${response}=    Post Request    reqbin_session    echo/post/json    headers=${header}    data=${postjson}
    ${status_code}=    Convert To String    ${response.status_code}
    Should Be Equal As Strings    ${status_code}    200
    Comment    Log    Delete operation being performed
    Comment    ${response}=    Delete Request    reqbin_session    echo/delete/json
    Comment    ${status_code}=    Convert To String    ${response.status_code}
    Comment    Should Be Equal As Strings    ${status_code}    200
    Comment    Log    ${response.text}
    Comment    Should Contain    ${response.text}    true
    Comment    Log    An example of getting the current weather data for a selected city
    Comment    ${header}=    Create Dictionary    Accept=application/json
    Comment    ${response}=    Get Request    reqbin_session    https://api.openweathermap.org/data/2.5/weather?appid={openweathermap_apikey}&q=chicago    headers=${header}
    Comment    ${status_code}=    Convert To String    ${response.status_code}
    Comment    Pass Execution    Step passed for conituation
    Comment    Run Keyword If    ${status_code}>0    Log    ${status_code}
    Comment    Log    ${response.text}
    Comment    create a new session for the URL "content ful"
    Create Session    contentful_session    ${Content_URL}
    ${content_header}=    Create Dictionary    Authorization='Bearer fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe'
    ${response}=    Get Request    contentful_session    spaces/yadj1kx9rmg0/environments/staging/tags/nyCampaign
    ${status_code}=    Convert To String    ${response.status_code}
    Log    ${response.text}
    Should Contain    ${status_code}    200

*** Keywords ***
add
    [Arguments]    ${num1}    ${num2}
    ${sum}=    Evaluate    int(${num1})+int(${num2})
    [Return]    ${sum}

API_Delete_Student
    Create Session    the_api_world    ${Base_URL}
    ${response}=    Delete Request    the_api_world    api/studentsDetails/177947
    Log    ${response.status_code}
    ${resp_code_str}=    Convert To String    ${response.status_code}
    Should Be Equal    ${resp_code_str}    200
    Log    ${response.content}

API_Put_Student
    Create Session    the_api_world    ${Base_URL}
    ${Body}=    Create Dictionary    id=177946    first_name=Deepa    middle_name=VL    last_name=Prakash    date_of_birth=15/04/1983
    ${Headers}=    Create Dictionary    Content-Type=application/json
    ${response}=    Put Request    the_api_world    api/studentsDetails/177946    data=${Body}    headers=${Headers}
    ${resp_code}=    Convert To String    ${response.status_code}
    Should Be Equal    ${resp_code}    200
    Log    ${resp_code}
    Log    ${response.content}

